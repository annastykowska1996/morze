def code_morze(morse_sentence):
    '''
    please add your solution here or call your solution implemented in different function from here  
    then change return value from 'False' to value that will be returned by your solution
    '''
    morse_dict = {'A': '.-', 'B': '-...', 'C': '-.-.', 'D': '-..', 'E': '.', 'F': '..-.', 'G': '--.', 'H': '....', 'I': '..', 'J': '.---', 'K': '-.-', 'L': '.-..', 'M': '--', 'N': '-.', 
         'O': '---', 'P': '.--.', 'Q': '--.-', 'R': '.-.', 'S': '...', 'T': '-', 'U': '..-', 'V': '...-', 'W': '.--', 'X': '-..-', 'Y': '-.--', 'Z': '--..','1': '.----', '2': '..---', '3': '...--','4': '....-', '5': '.....', '6': '-....',
         '7': '--...', '8': '---..', '9': '----.', '0': '-----', ', ': '--..--', 
         '.' : '.-.-.-', '?': '..--..', '/': '-..-.', '-': '-....-', '(': '-.--.', ')': '-.--.-'}
    morse_str = ''
    i = 1
    for letter in morse_sentence:
        if letter == ' ':
            continue
        else:
            morse_str = morse_str + morse_dict[letter.upper()]
            morse_str = morse_str + ' '
    print(morse_str)

    return morse_str[:-1]


